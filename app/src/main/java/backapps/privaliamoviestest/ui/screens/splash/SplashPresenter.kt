package backapps.privaliamoviestest.ui.screens.splash

import backapps.privaliamoviestest.ui.base.mvp.BasePresenter

/**
 * Created by javieralvarez on 7/1/18.
 */
class SplashPresenter(mView : SplashView) : BasePresenter<SplashView, SplashInteractor>(mView) {
    override val interactor = SplashInteractor()

    override fun init() {
        mView?.navigateHome()
    }
}