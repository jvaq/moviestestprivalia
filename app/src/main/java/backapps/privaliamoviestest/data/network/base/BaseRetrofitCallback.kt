package backapps.privaliamoviestest.data.network.base

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by javieralvarez on 8/1/18.
 */
class BaseRetrofitCallback<T>(private val serviceResponseListener : ServiceListener<T>) : Callback<T>  {


    override fun onFailure(call: Call<T>?, t: Throwable?) {
        serviceResponseListener.onError(ServiceException(t?.message))
    }

    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        if (response != null) {
            RetrofitResponseHandler.handleResponse(response,serviceResponseListener)
        }

    }
}