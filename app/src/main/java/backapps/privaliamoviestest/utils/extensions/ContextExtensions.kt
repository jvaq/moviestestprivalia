package backapps.privaliamoviestest.utils.extensions

import android.content.Context
import android.content.Intent
import backapps.privaliamoviestest.handlers.NavigationHandler

/**
 * Created by javieralvarez on 7/1/18.
 */
fun Context.navigateActivity(activityDestination : String){
    startActivity(Intent(this,    NavigationHandler.getActivityDestination(activityDestination)))
}