package backapps.privaliamoviestest.utils.images

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.widget.ImageView

/**
 * Created by javieralvarez on 8/1/18.
 */
interface ImageProvider {

    fun loadImage(context: Context, url: String, imageView: ImageView): ImageRequest

    fun loadImage(context: Context, url: String, imageView: ImageView, @DrawableRes errorHolder: Int): ImageRequest

}