package backapps.privaliamoviestest.data.network.base

import java.lang.Exception

/**
 * Created by javieralvarez on 8/1/18.
 */
class ServiceException(message: String?) : Exception(message) {
}