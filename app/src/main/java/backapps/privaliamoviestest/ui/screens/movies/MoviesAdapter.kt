package backapps.privaliamoviestest.ui.screens.movies

import backapps.privaliamoviestest.data.model.Movie
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import backapps.privaliamoviestest.R
import backapps.privaliamoviestest.utils.extensions.inflate
import backapps.privaliamoviestest.utils.extensions.loadImage
import backapps.privaliamoviestest.utils.images.ImageGenerator
import org.jetbrains.anko.find
import org.w3c.dom.Text

/**
 * Created by javieralvarez on 8/1/18.
 */
class MoviesAdapter (private val  moviesListener : MoviesListener)  : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

     var movies : MutableList<Movie> = mutableListOf()

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int)  = holder.bind(movies[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder = MoviesViewHolder(parent.inflate(R.layout.item_movie))

    override fun getItemCount(): Int = movies.size

    inner class MoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val movieImage: ImageView = itemView.find(R.id.iv_image)
        private val movieTitle: TextView = itemView.find(R.id.tv_title)
        private val movieOverview: TextView = itemView.find(R.id.tv_overview)
        private val movieYear: TextView = itemView.find(R.id.tv_year)


        fun bind(movie: Movie) = with(itemView){
            movie.poster_path?.let {movieImage.loadImage( ImageGenerator.generateImageUrl(it))}
            movieTitle.text = movie.title
            movieOverview.text = movie.overview
            movieYear.text = movie.release_date.split("-").first().toString()
            setOnClickListener { moviesListener.onMovieSelected(movie) }
        }
    }

    fun addMovies(newMovies : List<Movie>){
        movies.addAll(newMovies)
        notifyDataSetChanged()
    }

    fun setNewMovies(newMovies : List<Movie>){
        movies.clear()
        movies.addAll(newMovies)
        notifyDataSetChanged()

    }
}