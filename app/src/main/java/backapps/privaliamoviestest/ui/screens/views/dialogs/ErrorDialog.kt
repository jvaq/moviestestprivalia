package backapps.privaliamoviestest.ui.screens.views.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import backapps.privaliamoviestest.R
import kotlinx.android.synthetic.main.error_dialog.*

/**
 * Created by javieralvarez on 10/1/18.
 */
class ErrorDialog : DialogFragment() {

    private var message : String? = null

    fun setMessage(text : String){
        message = text
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.error_dialog,container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_message.text = message
    }
}