package backapps.privaliamoviestest.ui.screens.home

import backapps.privaliamoviestest.ui.base.mvp.BaseInteractor
import backapps.privaliamoviestest.ui.base.mvp.BasePresenter

/**
 * Created by javieralvarez on 7/1/18.
 */
class HomePresenter(mView : HomeView) : BasePresenter<HomeView,BaseInteractor>(mView) {

    override val interactor: BaseInteractor = HomeInteractor()

    override fun init() {
        mView?.initFragmentController()
    }
}