package backapps.privaliamoviestest.handlers

import android.content.Context
import android.widget.ImageView
import backapps.privaliamoviestest.utils.images.GlideImageProvider
import backapps.privaliamoviestest.utils.images.ImageProvider
import backapps.privaliamoviestest.utils.images.ImageRequest

/**
 * Created by javieralvarez on 8/1/18.
 */
class ImageHandler  private constructor() : ImageProvider {


    companion object {

        private var sInstance: ImageHandler? = null

        val instance: ImageHandler
            get() {
                if (sInstance == null) sInstance = ImageHandler()
                return sInstance!!
            }
    }

    private val mDelegate: ImageProvider

    init {
        mDelegate = GlideImageProvider()
    }

    override fun loadImage(context: Context, url: String, imageView: ImageView): ImageRequest {
        return mDelegate.loadImage(context,url,imageView)
    }

    override fun loadImage(context: Context, url: String, imageView: ImageView, errorHolder: Int): ImageRequest {
        return mDelegate.loadImage(context,url,imageView,errorHolder)
    }

}