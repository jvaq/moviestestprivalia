package backapps.privaliamoviestest.data.network.interfaces

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.ServiceListener
import retrofit2.Call

/**
 * Created by javieralvarez on 8/1/18.
 */
interface MoviesHandlerI {

    fun getPopularMovies(page : Int, listener: ServiceListener<PaginatedResult<Movie>>) : Call<PaginatedResult<Movie>>
}