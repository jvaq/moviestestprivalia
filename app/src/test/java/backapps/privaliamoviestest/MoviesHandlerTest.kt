package backapps.privaliamoviestest

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.ServiceException
import backapps.privaliamoviestest.data.network.base.ServiceListener
import backapps.privaliamoviestest.handlers.DataHandler
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CountDownLatch

/**
 * Created by javieralvarez on 11/1/18.
 */
class MoviesHandlerTest {

    @Test
    fun getPopularMovies(){
        val latch = CountDownLatch(1)
        DataHandler.init(BuildConfig.ENVIRONMENT)
        DataHandler.getInstance().moviesHandler.getPopularMovies(1,object : ServiceListener<PaginatedResult<Movie>> {
            override fun onSuccess(response: PaginatedResult<Movie>) {
                response.results?:Assert.assertTrue(true)
                latch.countDown()

            }

            override fun onError(serviceException: ServiceException) {
                Assert.assertFalse(true)
                latch.countDown()

            }

        })

        latch.await()
    }

    @Test
    fun getSearchMovies(){
        val latch = CountDownLatch(1)
        DataHandler.init(BuildConfig.ENVIRONMENT)
        DataHandler.getInstance().searchMoviesHandler.searchMovies("star",1,object : ServiceListener<PaginatedResult<Movie>> {
            override fun onSuccess(response: PaginatedResult<Movie>) {
                response.results?:Assert.assertTrue(true)
                latch.countDown()

            }

            override fun onError(serviceException: ServiceException) {
                Assert.assertFalse(true)
                latch.countDown()

            }

        })

        latch.await()
    }

}