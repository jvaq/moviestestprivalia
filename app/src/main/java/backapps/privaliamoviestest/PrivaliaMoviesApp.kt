package backapps.privaliamoviestest

import android.app.Application
import android.os.Build
import backapps.privaliamoviestest.handlers.DataHandler

/**
 * Created by javieralvarez on 7/1/18.
 */
class PrivaliaMoviesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DataHandler.init(BuildConfig.ENVIRONMENT)
    }
}