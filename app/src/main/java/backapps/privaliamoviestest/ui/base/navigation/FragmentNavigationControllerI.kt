package backapps.privaliamoviestest.ui.base.navigation

import android.support.v4.app.Fragment

/**
 * Created by javieralvarez on 7/1/18.
 */
interface FragmentNavigationControllerI {

    var currentTabIndex: Int
    var currentStackIndex : Int
    fun push(fragment: Fragment)
    fun pop()
    fun clearStack()
    fun switchStack(index : Int)
    fun onBackPressed(switchTabBlock : (tab : Int, mustExit: Boolean) -> Unit)

}