package backapps.privaliamoviestest.ui.base.mvp

/**
 * Created by javieralvarez on 7/1/18.
 */
abstract class BasePresenter<V : BaseView, I : BaseInteractor>(protected var mView: V?)  {

    protected var mInteractor: I

    protected abstract val interactor: I

    init {
        this.mInteractor = interactor
    }

    abstract fun init()

}
