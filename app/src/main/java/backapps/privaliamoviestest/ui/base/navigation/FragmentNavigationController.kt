package backapps.privaliamoviestest.ui.base.navigation

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import backapps.privaliamoviestest.ui.base.views.BaseFragment
import com.ncapdevi.fragnav.FragNavController

/**
 * Created by javieralvarez on 7/1/18.
 */
class FragmentNavigationController(rootFragments : List<Fragment>, containerId : Int, fragmentManager : FragmentManager, val transactionListener: ((BaseFragment?) -> Unit)? = null) : FragmentNavigationControllerI, FragNavController.TransactionListener {


    private val controller = FragNavController.newBuilder(null, fragmentManager,containerId)
            .rootFragments(rootFragments)
            .transactionListener(this)
            .build()

    override var currentTabIndex: Int  = 0
        get() = controller.currentStackIndex
    override var currentStackIndex: Int = 0
        get() = controller.currentStack?.size ?: 0


    override fun push(fragment: Fragment) {
        controller.pushFragment(fragment)
    }

    override fun pop() {
        controller.popFragment()
    }

    override fun clearStack() {
        controller.clearStack()
    }

    override fun switchStack(index: Int) {
        controller.switchTab(index )
    }

    override fun onBackPressed(switchTabBlock: (tab: Int, mustExit: Boolean) -> Unit) {
        if (controller.isRootFragment){
            if (currentTabIndex == 0) switchTabBlock(0,true)
            else {
                controller.switchTab(0)
                switchTabBlock(0, false)
            }
        }else{
            pop()
        }
    }

    override fun onFragmentTransaction(p0: Fragment?, p1: FragNavController.TransactionType?) {
        transactionListener?.invoke(p0 as? BaseFragment)
    }

    override fun onTabTransaction(p0: Fragment?, p1: Int) {
        transactionListener?.invoke(p0 as? BaseFragment)
    }
}