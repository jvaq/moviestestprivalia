package backapps.privaliamoviestest.ui.base.navigation

import backapps.privaliamoviestest.ui.base.views.BaseFragment

/**
 * Created by javieralvarez on 7/1/18.
 */
interface TransactionListener {
    fun onFragmentTransaction( fragment : BaseFragment? )
}