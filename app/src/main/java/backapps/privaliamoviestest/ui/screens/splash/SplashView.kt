package backapps.privaliamoviestest.ui.screens.splash

import backapps.privaliamoviestest.ui.base.mvp.BaseView

/**
 * Created by javieralvarez on 7/1/18.
 */
 interface SplashView : BaseView {

    fun showAnimation()
    fun navigateHome()
}