package backapps.privaliamoviestest.ui.screens.splash

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import backapps.privaliamoviestest.R
import backapps.privaliamoviestest.handlers.NavigationHandler
import backapps.privaliamoviestest.utils.extensions.navigateActivity

class SplashActivity : AppCompatActivity(), SplashView{

    private var ANIMATION_TIME : Long = 3000L
    private var presenter = SplashPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.init()
    }


    override fun showAnimation() {
        TODO("not implemented")
    }

    override fun navigateHome() {
        Handler().postDelayed(Runnable {
            applicationContext.navigateActivity(NavigationHandler.ActivityDestinations.HOME)
            finish()
        }, ANIMATION_TIME)    }

}
