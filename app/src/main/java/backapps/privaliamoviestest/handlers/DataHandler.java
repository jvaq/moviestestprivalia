package backapps.privaliamoviestest.handlers;

import android.app.Application;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import backapps.privaliamoviestest.data.network.base.ServiceGenerator;
import backapps.privaliamoviestest.data.network.handler.MoviesHandler;
import backapps.privaliamoviestest.data.network.handler.SearchMoviesHandler;
import backapps.privaliamoviestest.data.network.interfaces.MoviesHandlerI;
import backapps.privaliamoviestest.data.network.interfaces.SearchMoviesHandlerI;
import backapps.privaliamoviestest.data.network.services.MoviesService;
import backapps.privaliamoviestest.data.network.services.SearchMovieService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by javieralvarez on 8/1/18.
 */

public class DataHandler {

    private static DataHandler instance;
    public static DataHandler getInstance() {
        return instance;
    }

    private MoviesHandlerI moviesHandler;
    private SearchMoviesHandlerI searchMoviesHandler;

    public static void init(String environment) {
        if (instance == null)
        instance = new DataHandler(environment);
    }


    private DataHandler(String environment) {


        OkHttpClient httpClient = new OkHttpClient.Builder()
                .build();

        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(environment)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceGenerator serviceGenerator = new ServiceGenerator(retrofit);
        moviesHandler = new MoviesHandler(serviceGenerator.createService(MoviesService.class));
        searchMoviesHandler = new SearchMoviesHandler(serviceGenerator.createService(SearchMovieService.class));

    }

    public MoviesHandlerI getMoviesHandler() {
        return moviesHandler;
    }


    public SearchMoviesHandlerI getSearchMoviesHandler() {
        return searchMoviesHandler;
    }
}
