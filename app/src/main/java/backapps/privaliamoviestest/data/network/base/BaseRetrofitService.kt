package backapps.privaliamoviestest.data.network.base

/**
 * Created by javieralvarez on 8/1/18.
 */
open class BaseRetrofitService<T>(protected var service: T)
