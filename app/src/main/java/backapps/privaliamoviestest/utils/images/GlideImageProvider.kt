package backapps.privaliamoviestest.utils.images

import android.app.Activity
import android.content.Context
import android.os.Build
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Created by javieralvarez on 8/1/18.
 */
class GlideImageProvider : ImageProvider {


    override fun loadImage(context: Context, url: String, imageView: ImageView): ImageRequest {
        return if (!isContextValid(context)) NoOpImageRequest() else GlideImageRequest(Glide
                .with(context)
                .load(url)
                .into(imageView))

    }

    override fun loadImage(context: Context, url: String, imageView: ImageView, errorHolder: Int): ImageRequest {
        return if (!isContextValid(context)) NoOpImageRequest() else GlideImageRequest(Glide
                .with(context)
                .load(url)
                .error(errorHolder)
                .into(imageView))
    }

    private fun isContextValid(context: Context?): Boolean {
        return context != null && (context !is Activity || !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && context.isDestroyed))
    }
}