package backapps.privaliamoviestest.data.network.interfaces

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.ServiceListener
import retrofit2.Call

/**
 * Created by javieralvarez on 9/1/18.
 */
interface SearchMoviesHandlerI {

    fun searchMovies(query : String, page : Int, listener: ServiceListener<PaginatedResult<Movie>>) : Call<PaginatedResult<Movie>>
}