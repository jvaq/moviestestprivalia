package backapps.privaliamoviestest.data.network.base

import retrofit2.Retrofit

/**
 * Created by javieralvarez on 8/1/18.
 */
class ServiceGenerator(private val retrofit: Retrofit) {

    fun <T> createService(clazz: Class<T>): T {
        return retrofit.create(clazz)
    }
}