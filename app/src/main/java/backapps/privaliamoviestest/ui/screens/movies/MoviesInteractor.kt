package backapps.privaliamoviestest.ui.screens.movies

import android.bluetooth.BluetoothProfile
import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.ServiceListener
import backapps.privaliamoviestest.handlers.DataHandler
import backapps.privaliamoviestest.ui.base.mvp.BaseInteractor
import retrofit2.Call

/**
 * Created by javieralvarez on 8/1/18.
 */
class MoviesInteractor : BaseInteractor() {

    var previusCall : Call<PaginatedResult<Movie>>? = null

    fun getPopularMovies( page : Int, listener : ServiceListener<PaginatedResult<Movie>>){
        DataHandler.getInstance().moviesHandler.getPopularMovies(page,listener)
    }

    fun searchMovies(query : String, page : Int, listener: ServiceListener<PaginatedResult<Movie>>){
        previusCall?.cancel()
        previusCall =  DataHandler.getInstance().searchMoviesHandler.searchMovies(query,page,listener)
    }
}