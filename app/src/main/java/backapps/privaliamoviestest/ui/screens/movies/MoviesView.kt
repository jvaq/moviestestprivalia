package backapps.privaliamoviestest.ui.screens.movies

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.ui.base.mvp.BaseView

/**
 * Created by javieralvarez on 8/1/18.
 */
interface MoviesView : BaseView {

    fun addNewMovies(movies : List<Movie>)
    fun setMovies(movies: List<Movie>)
    fun showError(message : String)

}