package backapps.privaliamoviestest.handlers

import android.support.v4.app.Fragment
import backapps.privaliamoviestest.ui.screens.home.HomeActivity
import backapps.privaliamoviestest.ui.screens.movies.MoviesFragment
import backapps.privaliamoviestest.ui.screens.splash.SplashActivity

/**
 * Created by javieralvarez on 7/1/18.
 */
class NavigationHandler {

    companion object {


        fun getRootNavigationList(): List<Fragment> {
            return listOf(MoviesFragment()) // root fragment
        }

        fun getDestination(destination: String): Fragment? {

            when (destination) {
             Destinations.MOVIES ->  MoviesFragment()
            }
            return null
        }

        fun getActivityDestination(destination: String): Class<*>? {

            when (destination) {
                ActivityDestinations.HOME -> return HomeActivity::class.java
                ActivityDestinations.SPLASH -> return SplashActivity::class.java
            }
            return null
        }

    }

    object ActivityDestinations {
        const val SPLASH = "splash"
        const val HOME = "home"
    }

    object Destinations {
        const val MOVIES = "movies"
    }
}