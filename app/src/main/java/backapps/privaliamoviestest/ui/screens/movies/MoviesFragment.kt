package backapps.privaliamoviestest.ui.screens.movies


import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View

import backapps.privaliamoviestest.R
import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.ui.base.views.BaseFragment
import backapps.privaliamoviestest.ui.screens.views.dialogs.ErrorDialog
import backapps.privaliamoviestest.ui.screens.views.searchbar.SearchBar
import backapps.privaliamoviestest.utils.interfaces.InfiniteScrollListener
import kotlinx.android.synthetic.main.fragment_movies.*


/**
 * A simple [Fragment] subclass.
 */
class MoviesFragment : BaseFragment(), MoviesView, MoviesListener {

    override val layout: Int = R.layout.fragment_movies
    override val fragTitle: String = "Movies App"

    private var adapter: MoviesAdapter = MoviesAdapter(this)
    private val presenter: MoviesPresenter = MoviesPresenter(this)
    private lateinit var rvMovies: RecyclerView
    private lateinit var searchBar: SearchBar

    override fun bindViews(view: View) {
        rvMovies = rv_movies
        searchBar = search_bar
    }

    override fun loadData() {
        presenter.init()
        rvMovies.adapter = adapter

        val linearLayout = LinearLayoutManager(context)
        rvMovies.layoutManager = linearLayout

        rvMovies.addOnScrollListener(InfiniteScrollListener(linearLayout, { presenter.loadMoreMovies() }))
        searchBar.setSearchListener { presenter.searchMovies(it) }


    }

    override fun addNewMovies(movies: List<Movie>) {
        adapter.addMovies(movies)
    }

    override fun setMovies(movies: List<Movie>) {
        adapter.setNewMovies(movies)
        rvMovies.scrollToPosition(0)
    }

    override fun showError(message: String) {
        var dialog = ErrorDialog()
        dialog.setMessage(message)
        dialog.show(fragmentManager,"error")
    }

    override fun onMovieSelected(movie: Movie) {
    }


}
