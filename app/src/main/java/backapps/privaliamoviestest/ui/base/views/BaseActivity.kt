package backapps.privaliamoviestest.ui.base.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.View

/**
 * Created by javieralvarez on 7/1/18.
 */
abstract class BaseActivity : AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layout: Int

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(layout)
    }

    override fun setContentView(view: View) {
        throw IllegalStateException("Method not allowed")
    }

    override fun setContentView(layoutResID: Int) {
        throw IllegalStateException("Method not allowed")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onPostResume() {
        super.onPostResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}