package backapps.privaliamoviestest.utils.images

import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target

/**
 * Created by javieralvarez on 8/1/18.
 */
class GlideImageRequest(private val target: Target<*>) : ImageRequest {

    override fun cancel() {
        Glide.clear(target)
    }

}
