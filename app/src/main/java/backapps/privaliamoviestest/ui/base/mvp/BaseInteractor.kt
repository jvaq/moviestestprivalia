package backapps.privaliamoviestest.ui.base.mvp

import android.support.annotation.CallSuper
import retrofit2.Call
import java.util.ArrayList

/**
 * Created by javieralvarez on 7/1/18.
 */
abstract class BaseInteractor{

    private val calls : ArrayList<Any> = arrayListOf()

    protected fun addCall(call: Call<Any>) {
        this.calls.add(call)
    }

    @CallSuper
    fun onDestroy() {
    }
}