package backapps.privaliamoviestest.utils.extensions

import android.os.Bundle
import backapps.privaliamoviestest.handlers.NavigationHandler
import backapps.privaliamoviestest.ui.base.views.BaseFragment

/**
 * Created by javieralvarez on 7/1/18.
 */
fun BaseFragment.navigate(destination : String, bundle : Bundle){
    fragmentListener?.controller?.apply {
        NavigationHandler.getDestination(destination)?.apply {
            this.arguments = bundle
            push(this)
        }
    }
}