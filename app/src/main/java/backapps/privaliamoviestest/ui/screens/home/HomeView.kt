package backapps.privaliamoviestest.ui.screens.home

import backapps.privaliamoviestest.ui.base.mvp.BaseView

/**
 * Created by javieralvarez on 7/1/18.
 */
interface HomeView : BaseView {

    fun initFragmentController()
}