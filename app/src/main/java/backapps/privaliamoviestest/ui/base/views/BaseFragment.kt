package backapps.privaliamoviestest.ui.base.views

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by javieralvarez on 7/1/18.
 */
abstract class BaseFragment : Fragment() {


    @get:LayoutRes
    protected abstract val layout: Int
     abstract val fragTitle: String

    private var mRootView: View? = null
    private var mRecoverFromDetach = false
    var fragmentListener : BaseFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mRecoverFromDetach = mRootView != null
        if (mRootView == null)
            mRootView = inflater?.inflate(layout, container, false)
        return mRootView
    }

    override final fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        view?.let {
            if (!mRecoverFromDetach) {
                bindViews(it)
                loadData()
            }
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseFragmentListener) {
            fragmentListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        fragmentListener = null
    }

    /**
     * Called by parent to notify that views can be bind
     * @param view inflated View
     */
    protected abstract fun bindViews(view: View)


    /**
     * Called by parent to notify that it can load screen related data
     */
    protected abstract fun loadData()



}