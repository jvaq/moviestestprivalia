package backapps.privaliamoviestest.ui.base.views

import backapps.privaliamoviestest.ui.base.navigation.FragmentNavigationControllerI
import backapps.privaliamoviestest.ui.screens.views.navbar.MoviesNavBar

/**
 * Created by javieralvarez on 7/1/18.
 */
interface BaseFragmentListener {
    val controller : FragmentNavigationControllerI?
     val delegatedToolbar: MoviesNavBar?
}