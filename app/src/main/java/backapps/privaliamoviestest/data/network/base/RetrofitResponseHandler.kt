package backapps.privaliamoviestest.data.network.base

import com.google.gson.JsonSyntaxException
import retrofit2.Response
import java.io.IOException

/**
 * Created by javieralvarez on 8/1/18.
 */
class RetrofitResponseHandler {

    companion object {
        fun <T> handleResponse(response: Response<T>, listener: ServiceListener<T>) {
            if (response.isSuccessful) {
                listener.onSuccess(response.body())
            } else {
                listener.onError(ServiceException(response.message()))
            }
        }
    }


}