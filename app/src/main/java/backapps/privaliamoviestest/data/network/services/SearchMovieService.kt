package backapps.privaliamoviestest.data.network.services

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by javieralvarez on 9/1/18.
 */
interface SearchMovieService {

    @GET("search/movie")
    fun searchMovies(@Query("api_key") apikey : String, @Query("language") lang : String,@Query("query") query : String, @Query("page") page: Int) : Call<PaginatedResult<Movie>>


}