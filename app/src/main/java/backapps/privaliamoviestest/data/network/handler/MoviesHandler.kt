package backapps.privaliamoviestest.data.network.handler

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.BaseRetrofitCallback
import backapps.privaliamoviestest.data.network.base.BaseRetrofitService
import backapps.privaliamoviestest.data.network.base.ServiceConstants
import backapps.privaliamoviestest.data.network.base.ServiceListener
import backapps.privaliamoviestest.data.network.interfaces.MoviesHandlerI
import backapps.privaliamoviestest.data.network.services.MoviesService
import retrofit2.Call

/**
 * Created by javieralvarez on 8/1/18.
 */
class MoviesHandler(service : MoviesService) : MoviesHandlerI, BaseRetrofitService<MoviesService>(service) {

    override fun getPopularMovies(page: Int, listener: ServiceListener<PaginatedResult<Movie>>): Call<PaginatedResult<Movie>> {
        val call = service.getPopularMovies(ServiceConstants.MOVIES_API_KEY,ServiceConstants.LANG,page)
        call.enqueue(BaseRetrofitCallback(listener))
        return call
    }

}