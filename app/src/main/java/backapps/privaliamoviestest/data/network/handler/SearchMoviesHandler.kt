package backapps.privaliamoviestest.data.network.handler

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.BaseRetrofitCallback
import backapps.privaliamoviestest.data.network.base.BaseRetrofitService
import backapps.privaliamoviestest.data.network.base.ServiceConstants
import backapps.privaliamoviestest.data.network.base.ServiceListener
import backapps.privaliamoviestest.data.network.interfaces.SearchMoviesHandlerI
import backapps.privaliamoviestest.data.network.services.SearchMovieService
import retrofit2.Call

/**
 * Created by javieralvarez on 9/1/18.
 */
class SearchMoviesHandler(service: SearchMovieService) : SearchMoviesHandlerI, BaseRetrofitService<SearchMovieService>(service) {

    override fun searchMovies(query: String, page: Int, listener: ServiceListener<PaginatedResult<Movie>>) : Call<PaginatedResult<Movie>> {
        val call = service.searchMovies(ServiceConstants.MOVIES_API_KEY,ServiceConstants.LANG,query,page)
        call.enqueue(BaseRetrofitCallback(listener))
        return call
    }
}