package backapps.privaliamoviestest.data.model

/**
 * Created by javieralvarez on 8/1/18.
 */
data class PaginatedResult<T> (
    var page: Int?,
    var total_pages : Int?,
    var results : List<T>? )
