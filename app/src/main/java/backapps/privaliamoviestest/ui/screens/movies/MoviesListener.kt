package backapps.privaliamoviestest.ui.screens.movies

import backapps.privaliamoviestest.data.model.Movie

/**
 * Created by javieralvarez on 8/1/18.
 */
interface MoviesListener {
    fun onMovieSelected(movie : Movie)
}