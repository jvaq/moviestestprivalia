package backapps.privaliamoviestest.data.network.base

/**
 * Created by javieralvarez on 8/1/18.
 */
interface ServiceListener<T> {
    fun onSuccess(response : T)
    fun onError(serviceException : ServiceException)
}