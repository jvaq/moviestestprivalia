package backapps.privaliamoviestest.ui.screens.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import backapps.privaliamoviestest.R
import backapps.privaliamoviestest.handlers.NavigationHandler
import backapps.privaliamoviestest.ui.base.navigation.FragmentNavigationController
import backapps.privaliamoviestest.ui.base.navigation.FragmentNavigationControllerI
import backapps.privaliamoviestest.ui.base.views.BaseActivity
import backapps.privaliamoviestest.ui.base.views.BaseFragmentListener
import backapps.privaliamoviestest.ui.screens.views.navbar.MoviesNavBar
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), BaseFragmentListener,HomeView {


    override val layout: Int = R.layout.activity_home

    private var presenter = HomePresenter(this)
    private lateinit var toolbar: MoviesNavBar
    private var fragmentNavigationController: FragmentNavigationControllerI? = null


    override val controller: FragmentNavigationControllerI?
        get() = fragmentNavigationController
    override val delegatedToolbar: MoviesNavBar?
        get() = toolbar



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getLayoutViews()
        presenter.init()
    }

    private fun getLayoutViews() {
        toolbar = mn_toolbar
    }


    override fun initFragmentController() {
        fragmentNavigationController = FragmentNavigationController(NavigationHandler.getRootNavigationList(),
                R.id.contentContainer,
                supportFragmentManager,
                { fragment ->
                    fragment?.apply {
                        toolbar.showBackButton(fragmentNavigationController?.currentStackIndex ?: 0 > 1)
                        toolbar.changeTitle(fragTitle)
                    }

                }
        )
    }
}
