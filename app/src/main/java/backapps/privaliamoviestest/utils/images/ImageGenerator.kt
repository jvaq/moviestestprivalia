package backapps.privaliamoviestest.utils.images

/**
 * Created by javieralvarez on 8/1/18.
 */
class ImageGenerator {


    companion object {
        private  var IMAGE_STRING = "https://image.tmdb.org/t/p/w500/"

        fun  generateImageUrl(path : String): String {
            return IMAGE_STRING + path
        }
    }
}