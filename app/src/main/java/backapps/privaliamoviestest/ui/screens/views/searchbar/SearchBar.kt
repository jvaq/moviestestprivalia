package backapps.privaliamoviestest.ui.screens.views.searchbar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import backapps.privaliamoviestest.R
import backapps.privaliamoviestest.utils.extensions.afterTextChanged
import backapps.privaliamoviestest.utils.extensions.gone
import backapps.privaliamoviestest.utils.extensions.visible
import kotlinx.android.synthetic.main.searchbar.view.*

/**
 * Created by javieralvarez on 9/1/18.
 */
class SearchBar  @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    private var etSearch : EditText
    private var ivSearch : ImageView

   private lateinit var searchListener : (String) ->Unit

    init {
        View.inflate(context, R.layout.searchbar, this)
        etSearch = et_search
        ivSearch = iv_search
        etSearch.afterTextChanged {
            searchListener.invoke(it)
            if (it.isEmpty()){
                ivSearch.visible()
            }else{
                ivSearch.gone()
            }
        }

    }

    fun setSearchListener( listener : (String) -> Unit){
        searchListener = listener
    }

}
