package backapps.privaliamoviestest.data.model

import java.util.*

/**
 * Created by javieralvarez on 8/1/18.
 */
data class Movie(
        var poster_path : String?,
        var overview : String,
        var release_date : String,
        var id : String,
        var original_title : String,
        var title : String
)