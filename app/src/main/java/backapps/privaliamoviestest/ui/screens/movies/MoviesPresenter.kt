package backapps.privaliamoviestest.ui.screens.movies

import backapps.privaliamoviestest.data.model.Movie
import backapps.privaliamoviestest.data.model.PaginatedResult
import backapps.privaliamoviestest.data.network.base.ServiceException
import backapps.privaliamoviestest.data.network.base.ServiceListener
import backapps.privaliamoviestest.ui.base.mvp.BasePresenter

/**
 * Created by javieralvarez on 8/1/18.
 */
class MoviesPresenter(moviesView: MoviesView) : BasePresenter<MoviesView, MoviesInteractor>(moviesView), ServiceListener<PaginatedResult<Movie>> {


    override val interactor: MoviesInteractor = MoviesInteractor()
    private var lastPage: Int = 1
    private var searching: Boolean = false
    private var lastQuery: String = ""

    override fun init() {

        interactor.getPopularMovies(lastPage, this)
    }


    override fun onSuccess(response: PaginatedResult<Movie>) {
        response.page?.let { lastPage = it }
        response.results?.apply { mView?.addNewMovies(this) }

    }

    override fun onError(serviceException: ServiceException) {
        //mView?.showError(serviceException.message!!)
    }

    fun loadMoreMovies() {
        if (!searching) {
            interactor.getPopularMovies(++lastPage, this)
        } else {
            interactor.searchMovies(lastQuery,++lastPage, this)
        }
    }

    fun searchMovies(query: String) {
        lastQuery = query
        lastPage = 1

        if (query.isEmpty()){
            searching = false
            interactor.getPopularMovies(lastPage,object : ServiceListener<PaginatedResult<Movie>>{
                override fun onSuccess(response: PaginatedResult<Movie>) {
                    response.page?.let { lastPage = it }
                    response.results?.apply { mView?.setMovies(this) }

                }

                override fun onError(serviceException: ServiceException) {
                   // mView?.showError(serviceException.message!!)
                }
            })

        }else{
            searching = true
            interactor.searchMovies(query, lastPage, object : ServiceListener<PaginatedResult<Movie>>{
                override fun onSuccess(response: PaginatedResult<Movie>) {
                    response.page?.let { lastPage = it }
                    response.results?.apply {

                        if (this.isEmpty()){
                            mView?.showError("Sorry, Not Movies found with "+ lastQuery)
                        }
                        mView?.setMovies(this)
                    }

                }

                override fun onError(serviceException: ServiceException) {
                    //mView?.showError(serviceException.message!!)

                }
            })
        }


    }
}